package main

import (
	"encoding/json"
	"fmt"
	"git.thm.de/bahn-simulator/libtrainsim-go"
	"io/ioutil"
	"log"
)

var supportedVersions = [...]libtrainsim.VersionStruct { {Major:0, Minor:4, Patch:0} }

func main(){

	fmt.Println("Welcome to the train creator.")
	fmt.Println("\nFirst you will have to select which format version you want to target.")
	fmt.Println("The format version determines the features that can be used.")
	fmt.Println("It is recommended to use the latest version if possible.")

	selectedVersion := supportedVersions[getFormatVersionIndex()]
	fmt.Printf("You have selected the format Version {%d.%d.%d}\n",selectedVersion.Major,selectedVersion.Minor,selectedVersion.Patch)
	create(selectedVersion)
}

func create(version libtrainsim.VersionStruct){
	var trainConfig libtrainsim.TrainProperties

	switch version {
	case libtrainsim.VersionStruct{Major:0, Minor:4, Patch:0}:
		tmp := createTrainProperties040()
		trainConfig = tmp.Cast()
	default:
		trainConfig = createTrainProperties()
	}

	if !trainConfig.IsValid(){
		fmt.Println("The train configuration is not valid. Restart the program and try again!")
		return
	}

	filename := trainConfig.Name + ".json"
	jOutput, err := json.MarshalIndent(trainConfig, "", "     ")
	if err != nil {
		log.Fatalln(err.Error())
		return
	}

	err = ioutil.WriteFile(filename, jOutput, 0644)

	if err != nil {
		log.Fatalln(err.Error())
		return
	}

	fmt.Println("Created the train file " + filename)
}
