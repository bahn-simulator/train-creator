package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func getFormatVersionIndex() int{
	fmt.Println("\n\nThe available format Versions are:")

	for i := 0; i < len(supportedVersions); i++ {
		fmt.Printf("[%d] -> {%d.%d.%d}\n",i, supportedVersions[i].Major,supportedVersions[i].Minor,supportedVersions[i].Patch)
	}

	fmt.Println("Inside of the brackets [] are the indices, which are needed to select the version.")

	var input int = 0
	scanner := bufio.NewScanner(os.Stdin)
	var err error = nil

	for ok := true; ok; {
		fmt.Println("Please enter the index of the version you want to use:")
		scanner.Scan()
		input, err = strconv.Atoi(scanner.Text())

		if err != nil || input < 0 || input >= len(supportedVersions) {
			// handle invalid input
			fmt.Println("please enter a valid value")
			continue
		}

		ok = false
		fmt.Printf("\n\n")
	}

	return input
}

func readTrainName() string{
	fmt.Println("Enter the name of the train:")

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()

	fmt.Printf("\n\n")
	return scanner.Text()
}

func readPositiveFloat(message string) float64{
	var input float64 = 0
	scanner := bufio.NewScanner(os.Stdin)
	var err error = nil

	for ok := true; ok; {
		fmt.Println(message)
		fmt.Println()
		scanner.Scan()
		input, err = strconv.ParseFloat(scanner.Text(),64)

		if err != nil || input < 0 {
			// handle invalid input
			fmt.Println("please enter a valid value")
			continue
		}

		ok = false
		fmt.Printf("\n\n")
	}

	return input
}

func readVelocityUnit() string{
	fmt.Println("The velocity unit can have the following units:")
	fmt.Println("    ms -> meter per second (default value)")
	fmt.Println("    kmh -> kilometer per hour")
	fmt.Println("Anything else will use the default unit.")
	fmt.Println("\nEnter the velocity unit of the train:")

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()

	fmt.Printf("\n\n")
	if strings.Contains(scanner.Text(), "kmh") {
		return "kmh"
	}

	return "ms"
}