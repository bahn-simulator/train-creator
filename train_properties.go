package main

import (
	"git.thm.de/bahn-simulator/libtrainsim-go"
)

func createTrainProperties() libtrainsim.TrainProperties{
	tmp := createTrainProperties040()
	return tmp.Cast()
}

func createTrainProperties040() libtrainsim.TrainProperties040{
	var retVal libtrainsim.TrainProperties040
	retVal.FormatVersion = "0.4.0"
	retVal.Name = readTrainName()
	retVal.Mass = readPositiveFloat("Please enter the mass of the train:")
	retVal.VelocityUnit = readVelocityUnit()
	retVal.MaxVelocity = readPositiveFloat("Please enter the maximum velocity of the train:")
	retVal.MaxAcceleration = readPositiveFloat("Please enter the maximum acceleration of the train:")
	retVal.TrackDrag = readPositiveFloat("Please enter the drag coefficient of the train with the track:")
	retVal.AirDrag = readPositiveFloat("Please enter the air drag coefficient of the train:")

	return retVal
}
